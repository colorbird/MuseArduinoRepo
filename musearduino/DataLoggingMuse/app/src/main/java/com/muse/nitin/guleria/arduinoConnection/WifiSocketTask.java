package com.muse.nitin.guleria.arduinoConnection;

import android.os.*;
import android.os.Process;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Created by nitinguleria on 15-07-21.
 * Background Thread that reads and writes to the external storage file system in the default sdcard/ directory
 */

public class WifiSocketTask extends HandlerThread {

    private Handler backgroundAssistant; //handler is a worker or backgroundAssistant

    private static final int READ=1;
    private static final int WRITE=2;

    //wifi variables
    private String ipAdress="";
    private int port=0;
    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    String line;
    int timeOut=500000;
    int counter =0;


    Boolean arduinoConnectedWifi =true;

   // String museData="0";

    public WifiSocketTask(String ipAdress,int port) {

        super("SavetoFileThread", Process.THREAD_PRIORITY_BACKGROUND);
        this.ipAdress= ipAdress;
        this.port= port;

    }

    @Override
    protected void onLooperPrepared(){
        super.onLooperPrepared();
        backgroundAssistant = new Handler(getLooper()){

            @Override
            public void handleMessage(Message msg){
                switch (msg.what){
                    case READ:
                        // backgroundAssistant.sendMessage()
                        break;
                    case WRITE:
                        String museData= (String) msg.obj;
                    if(!arduinoConnectedWifi) {
                        startSocket(ipAdress, port, museData);
                    }
                        break;
                }
            }
        };
    }

    /*
    * Initiate a read from the UI thread
    */
    public void read(){
        backgroundAssistant.sendEmptyMessage(READ);
    }
    /*
        * Write values from the UI thread
        */
    public void write(String writeText)
    {
        backgroundAssistant.sendMessage(Message.obtain(Message.obtain(backgroundAssistant, WRITE, writeText)));
    }


    public void startSocket(String ipAdress,int port, String museDataWritten){

        try {
        if(!arduinoConnectedWifi) {
            SocketAddress socketAddress = new InetSocketAddress(ipAdress, port);
            socket = new Socket();
            socket.connect(socketAddress,timeOut);
            Log.i("WifiSocketThread", "socket connected " );


            arduinoConnectedWifi=true;
         }
         if(socket!=null) {
           //  if (socket.isConnected()) {
                 inputStream = socket.getInputStream();
                 outputStream = socket.getOutputStream();
                 Log.i("WifiSocketThread", ": Socket created, streams assigned");
                // Log.i("WifiSocketThread", "Waiting for initial muse data..." + museDataWritten);
              //counter ++;
                // if(counter>2) {
                    Log.i("WifiSocketThread", "writing to stream with value : " + museDataWritten);

                     outputStream.write(museDataWritten.getBytes());
                  // counter=0;
               //}

                 //handshake
               /* BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
             //    StringBuilder total = new StringBuilder();
                 while ((line = r.readLine()) != null) {
                     if(line=="1"){
                         Log.i("WifiSocketThread", "Waiting for initial muse data..." + museDataWritten);
                         outputStream.write(museDataWritten.getBytes());
                     }
                 }
*/
            // }
             /*else if(socket.isClosed()|| socket.isOutputShutdown()){
                 //arduinoConnectedWifi=false;
             }*/
         }


        } catch (IOException e) {
            e.printStackTrace();
            Log.i("WifiSocketThread", "Connection operation 1 : IOException");
        }catch (Exception e) {
            e.printStackTrace();
            Log.i("WifiSocketThread", "Connection operation 1 : Exception");
        }

        //socket disconnected hence close it

        try {
            if (socket != null) socket.close();
            if (inputStream != null) inputStream.close();
            if (outputStream != null) outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    //getter and setter

    public Boolean getArduinoConnectedWifi() {
        return arduinoConnectedWifi;
    }

    public void setArduinoConnectedWifi(Boolean arduinoConnectedWifi) {
        this.arduinoConnectedWifi = arduinoConnectedWifi;
    }





}


