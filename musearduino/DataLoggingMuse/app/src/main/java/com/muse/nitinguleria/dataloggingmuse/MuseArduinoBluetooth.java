package com.muse.nitinguleria.dataloggingmuse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.view.View.OnClickListener;

import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.Muse;
import com.interaxon.libmuse.MuseDataPacketType;
import com.interaxon.libmuse.MuseManager;
import com.interaxon.libmuse.MusePreset;
import com.muse.nitin.guleria.arduinoConnection.BlueComms;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import commusenitinguleriaMuseListeners.ConnectionListener;
import commusenitinguleriaMuseListeners.DataListener;


/*
* Change Arduino Bluetooth
*
* */

public class MuseArduinoBluetooth extends Activity implements OnClickListener {


    DataListener dataListener;
    ConnectionListener connectionListener;


    public BlueComms myBlueComms;
    private ToggleButton connect, sendtoggleButton;
    private TextView textView;

    //Muse elements
    Button refreshButton;
    ToggleButton MuseConnectButton;
    Spinner museSpinner;

    //Muse library
    private Muse muse= null;
    //can be used to make pause /[play
    private boolean dataTransmission = true;

    //UI button wifi
    ToggleButton bluetoothConnect;

    public MuseArduinoBluetooth(){
        //listeners run on this activity
        WeakReference<Activity> weakActivity=new WeakReference<Activity>(this);
        connectionListener = new ConnectionListener(weakActivity);
        dataListener = new DataListener(weakActivity);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muse_arduino_bluetooth);
        myBlueComms = new BlueComms();
        connect =(ToggleButton) findViewById(R.id.bluetoothConnect);
        textView=(TextView) findViewById(R.id.bluetoothStatusTextView);
        sendtoggleButton=(ToggleButton) findViewById(R.id.sendtoggleButton);


        //Muse Buttons
        refreshButton=(Button) findViewById(R.id.refreshButton);
        MuseConnectButton =(ToggleButton) findViewById(R.id.museConnectButton);
        //MuseconnectButtonClicked function defined
        museSpinner = (Spinner) findViewById(R.id.museSpinner);

        // attach listener
        refreshButton.setOnClickListener(this);


        dataListener.setBluetoothActivity(true);

    }

    public void bluetoothConnectButtonClicked(View view){

        boolean on = ((ToggleButton) view).isChecked();
        if(on){

            dataListener.setBluetoothconnected(true);
            textView.setText("Bulb connected");

           /* try{
               myBlueComms.findBT();
               myBlueComms.openBT();
            } catch (IOException ex){
                showMessage("Connect Failed due to IOException");
            }*/
        }
        else{
            textView.setText("Bulb disconnected");
            dataListener.setBluetoothconnected(true);

/*
            try{
               myBlueComms.closeBT();
            } catch (IOException ex){
                showMessage("Close Failed due to IOException");
            }*/

        }

    }



    ////send button
    public void sendButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {

            dataListener.setBluetoothsendData(true);
            /*try{
                myBlueComms.sendData("0");
                myBlueComms.openBT();
            } catch (IOException ex){
                showMessage("Connect Failed due to IOException");
            }*/
        } else {
            dataListener.setBluetoothsendData(false);

        }
    }

    private void showMessage(String theMsg) {
        Toast msg = Toast.makeText(getBaseContext(),
                theMsg, Toast.LENGTH_LONG);
        msg.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_muse_arduino_bluetooth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_record:
                intent = new Intent(this,DataRecording.class);
                startActivity(intent);
                return true;

            case R.id.action_wifi:
                intent = new Intent(this,MuseArduinoWifi.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    *  public void OnClickOpen(View v){
        try{
            ((cBaseApplication)this.getApplicationContext()).myBlueComms.findBT();
            ((cBaseApplication)this.getApplicationContext()).myBlueComms.openBT();
        } catch (IOException ex){
            showMessage("Connect Failed due to IOException");
        }
    }
    public void OnClickClose(View v) {
        try{
            ((cBaseApplication)this.getApplicationContext()).myBlueComms.closeBT();
        } catch (IOException ex){
            showMessage("Close Failed due to IOException");
        }
    }
    public void OnClickExit(View v){
        finish();
        System.exit(0);
    }
    private void showMessage(String theMsg) {
        Toast msg = Toast.makeText(getBaseContext(),
                theMsg, Toast.LENGTH_LONG);
        msg.show();
    }
*/


    //implement toggle button clicks

    //connect button
    public void MuseconnectButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            connectMuse();
        } else {
            if (muse != null) {
                muse.disconnect(true);
            }
        }
    }




    @Override
    public void onClick(View v){

        if(v.getId()==R.id.refreshButton){

            //change the background to X
            refreshButton.setBackgroundResource(R.drawable.cancelrefresh);
            //refresh
            MuseManager.refreshPairedMuses();

            //list of names
            List<Muse> pairedMuses = MuseManager.getPairedMuses();
            List<String> spinnerItems = new ArrayList<String>();

            //get names
            for (Muse m : pairedMuses) {
                String dev_id = m.getName() + "-" + m.getMacAddress();
                Log.i("Muse Headband", dev_id);
                spinnerItems.add(m.getName());

            }

            //insert names in spinner
            ArrayAdapter<String> adapterArray = new ArrayAdapter<String>(
                    this, android.R.layout.simple_spinner_item, spinnerItems);
            museSpinner.setAdapter(adapterArray);

            //change the background back to refresh icon
            refreshButton.setBackgroundResource(R.drawable.refresh);

        }


    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        dataListener.setBluetoothActivity(false);

    }
    @Override
    protected void onPause(){
        super.onPause();
        dataListener.setBluetoothActivity(false);

    }

    @Override
    protected void onResume(){
        super.onResume();
        dataListener.setBluetoothActivity(true);

    }




    // Connected State of Muse
    public void connectMuse(){
        List<Muse> pairedMuses = MuseManager.getPairedMuses();
        if (pairedMuses.size() < 1 ||
                museSpinner.getAdapter().getCount() < 1) {
            Log.w("Muse Headband", "There is nothing to connect to");
        } else {
            muse = pairedMuses.get(museSpinner.getSelectedItemPosition());
            ConnectionState state = muse.getConnectionState();
            if (state == ConnectionState.CONNECTED ||
                    state == ConnectionState.CONNECTING) {
                Log.w("Muse Headband", "doesn't make sense to connect second time to the same muse");
                return;
            }
            configure_library();
            /**
             * In most cases libmuse native library takes care about
             * exceptions and recovery mechanism, but native code still
             * may throw in some unexpected situations (like bad bluetooth
             * connection). Print all exceptions here.
             */
            try {
                muse.runAsynchronously();
            } catch (Exception e) {
                Log.e("Muse Headband", e.toString());

            }
        }
    }

    //helper  function for connectMuse()
    private void configure_library() {
        muse.registerConnectionListener(connectionListener);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ACCELEROMETER);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.EEG);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ARTIFACTS);

        muse.setPreset(MusePreset.PRESET_14);
        muse.enableDataTransmission(dataTransmission);
    }
}
